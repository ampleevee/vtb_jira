package main

import (
	"fmt"
	"vtb_jira/dir_functions"
	"vtb_jira/my_scrum"
)

func main() {

	statusIndex:=5
	assigneeIndex:=9
	spIndex:=4

	//dir := "./data/pi_5_2020_s1"
	//dir := "./data/phenix/pi_5_2020_s1"
	dir := "./data/vtb/elbek/sprint_2021_2_5"
	//dir := "./data/vtb/stream_test"
	//dir := "./data/for_presentation"

	//sprintStart, sprintFinish := my_time.ConvToTimeSprint("02-09-2019_09-00-00", "13-09-2019_18-00-00")
	//my_scrum.PrintAllBurnDownPoints(allCsvFiles, sprintStart, sprintFinish, dir)

	allCsvFiles := dir_functions.SearchAllCsvFiles(dir)
	team := my_scrum.GetSprintTeam(allCsvFiles, dir, assigneeIndex)
	// на этом этапе в тим у нас вся команда текущего спринта без посчитанных сп

	theLastFilePath := allCsvFiles[len(allCsvFiles)-1:]
	// здесь мы получили последний актуальный файл

	fmt.Println("---------------------------")
	fmt.Println("--------TEAM RATING--------")
	fmt.Println("---------------------------")
	my_scrum.PrintCurrentTeamRating(team, theLastFilePath[0], dir, assigneeIndex, statusIndex, spIndex)
	//my_scrum.PrintCurrentTeamRatingForChart(team, theLastFilePath[0], dir)

	fmt.Println("---------------------------")
	fmt.Println("-----SPRINT CUMULATIVE-----")
	fmt.Println("---------------------------")
	my_scrum.PrintAllSpByStatusesForChart(dir, statusIndex, spIndex)
	fmt.Println("-------------------")
	fmt.Println("-----BURN DOWN-----")
	fmt.Println("-------------------")
	my_scrum.PrintBurnDownChart(dir, statusIndex, spIndex)
	fmt.Println("-------------------------")
	fmt.Println("-----FULL CUMULATIVE-----")
	fmt.Println("-------------------------")

	//my_scrum.PrintFullCumulativeChart("./data/full_data")

	//time.Sleep(300 * time.Second)

}
