package my_conv

import (
	"log"
	"strconv"
	"strings"
)

func ThisStrToFloat(a string) float64 {
	if (a == "") {
		return 0
	}
	a = strings.Replace(a, ",", ".", 1)
	b, err := strconv.ParseFloat(a, 64)
	if err != nil {
		log.Fatal(err)
	}
	return b/60/60/8
}
