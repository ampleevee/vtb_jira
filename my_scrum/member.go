package my_scrum

import (
	"fmt"
	"sort"
	"strings"
	"vtb_jira/my_conv"
	"vtb_jira/my_csv"
)

type Member struct {
	Name    string
	SpBurnt float64
	SpAll   float64
}

func GetSprintTeam(allFiles []string, dir string, assigneeIndex int) []Member {

	var sprintTeam []Member

	//sprintTeam = append(sprintTeam, Member{"Vasia",1,1})
	// перебираем все файлы, которые есть
	for i := 0; i < len(allFiles); i++ {

		fileData := my_csv.CsvParse(allFiles[i], dir)

		for j := 1; j < len(fileData); j++ {

			// вызываем функцию, которая проверит есть ли в массиве уже данный член команды.
			// И, если нет, до добавит его в результирующий массив
			//memberName := ConvertName(fileData[j][10])
			//fileData[j][10] := memberName
			sprintTeam = AddTeamMember(sprintTeam, fileData[j][assigneeIndex])

			// также нам необходимо из 8 ячейки вычленить всех
			// участников, которые перечислены через запятую
			// Для этого вызовем функцию и передадим ей полное значение, а она вернет нам строковый массив,
			// который мы также последовательно прогоним

			//membersAsSlice := popTheMembersFromAdditionalMembers(fileData[j][6])
			//
			//for k := 0; k < len(membersAsSlice); k++ {
			//
			//	if membersAsSlice[k] != "" {
			//
			//		sprintTeam = AddTeamMember(sprintTeam, membersAsSlice[k])
			//
			//	}
			//
			//}

		}

	}

	return sprintTeam

}

func popTheMembersFromAdditionalMembers(members string) []string {

	// преобразуем строку с запятыми в срез и убираем лишние пробелы
	result := strings.Split(members, ",")
	for i := 0; i < len(result); i++ {
		result[i] = strings.Trim(result[i], " ")
	}
	return result

}

func AddTeamMember(sprintTeam []Member, memberName string) []Member {

	// если срез пустой, то просто добавляем первого члена команды
	if len(sprintTeam) == 0 && memberName != "" {
		sprintTeam = append(sprintTeam, Member{memberName, 0, 0})
		return sprintTeam
	}

	isMemberAlready := false

	for i := 0; i < len(sprintTeam); i++ {

		//ниже условие для отладки
		if sprintTeam[i].Name == "" {
			fmt.Println("empty sprintTeam[i]", sprintTeam[i])
		}

		if sprintTeam[i].Name == memberName || sprintTeam[i].Name == "" {
			isMemberAlready = true
		}

	}

	if !isMemberAlready && memberName != "" && memberName != "Ильдинбаев Бахтияр Казамханович" && memberName != "Амплеев Евгений Михайлович" && memberName != "Кайнов Илья Михайлович" && memberName != "Морозов Роман Александрович" && memberName != "Хабибуллин Дамир Наилевич" {
		sprintTeam = append(sprintTeam, Member{memberName, 0, 0})
	}

	return sprintTeam
}

func PrintCurrentTeamRating(team []Member, lastFile string, dir string, assigneeIndex, statusIndex, spIndex int) {

	// здесь получили текущий рейтинг на последний актуальный файл с данными
	//fmt.Println("Запустили GetCurrentTeamRating")
	team = GetCurrentTeamRating(team, lastFile, dir, statusIndex, spIndex, assigneeIndex)
	//fmt.Println("=========team=")
	//fmt.Println(team)
	//fmt.Println("=========end team=")

	// дальше просто выводим рейтинг каждого и параллельно считаем в динамике
	for i := 0; i < len(team); i++ {

		//fmt.Printf("%v место: %v (сожжено %v из %v)\n", i+1, team[i].Name, team[i].SpBurnt, team[i].SpAll)

		fmt.Printf(" %v место: %v (сожжено %v из %v)", i+1, team[i].Name, team[i].SpBurnt, team[i].SpAll)
		fmt.Printf(" динамика(")

		dynamics := GetDynamics(team[i], dir, assigneeIndex, statusIndex, spIndex)

		for j := 0; j < len(dynamics); j++ {
			if j != len(dynamics)-1 {
				fmt.Printf("%v->", dynamics[j])
			} else {
				fmt.Printf("%v)\n", dynamics[j])
			}

		}
	}

}

func PrintCurrentTeamRatingForChart(team []Member, lastFile string, dir string, assigneeIndex, statusIndex, spIndex int) {

	team = GetCurrentTeamRating(team, lastFile, dir, statusIndex, spIndex, assigneeIndex)

	for i := 0; i < len(team); i++ {

		dynamics := GetDynamics(team[i], dir, assigneeIndex, statusIndex, spIndex)

		fmt.Printf("{\n")
		fmt.Printf("label: '%v',\n", team[i].Name)

		switch team[i].Name {

		case "Шувалов Борис Викторович":
			fmt.Printf("borderColor: '#DF6262',\n")

		case "Пушина Наталья Сергеевна":
			fmt.Printf("borderColor: '#DF9B62',\n")

		case "Морозов Роман Александрович":
			fmt.Printf("borderColor: '#B34F87',\n")

		case "Фёклин Александр Александрович":
			fmt.Printf("borderColor: '#4FB34F',\n")

		case "Григорьев Константин Владимирович":
			fmt.Printf("borderColor: '#4B6195',\n")

		case "Никитина Алла Гургеновна":
			fmt.Printf("borderColor: '#5F4D9A',\n")

		case "Орлова Наталья Валерьевна":
			fmt.Printf("borderColor: '#3B8686',\n")

		case "Киклевич Андрей Олегович":
			fmt.Printf("borderColor: '#DFB562',\n")

		case "Пушкарь Илья Андреевич":
			fmt.Printf("borderColor: '#36869A',\n")

		case "Аркадий":
			fmt.Printf("borderColor: '#36869A',\n")

		case "Василий":
			fmt.Printf("borderColor: '#DFB562',\n")

		case "Татьяна":
			fmt.Printf("borderColor: '#5F4D9A',\n")

		case "Ларионов Константин Александрович":
			fmt.Printf("borderColor: '#DF9B62',\n")

		}

		fmt.Printf("backgroundColor: 'rgba(96,255,87,0.0)',\n")

		fmt.Printf("pointRadius: 6,\n")
		fmt.Printf("data: [")

		for i := 0; i < len(dynamics); i++ {

			if i != len(dynamics)-1 {
				fmt.Printf("%v, ", len(team)-dynamics[i]+1)
			} else {
				fmt.Printf("%v]\n", len(team)-dynamics[i]+1)
			}

		}
		fmt.Printf("\n},\n")
	}

}

func GetCurrentTeamRating(team []Member, lastFile string, dir string, statusIndex, spIndex, assigneeIndex int) []Member {



	// здесь проходим в цикле каждого члена команды
	for i := 0; i < len(team); i++ {

		team[i] = updateTeamMemberRating(team[i], lastFile, dir, statusIndex, spIndex, assigneeIndex)

	}


	sort.Slice(team, func(i, j int) bool {

		// Сначала смотрим - все ли сп закрыты и, если да, то во сколько раз один участник активнее в спринте, чем другой.
		// На сколько sp влияет. Например, один может влиять на 2SP, а другой на 11SP. Не совсем логично отдавать первое место
		// тому кто выполнил 2 из 2, если второй участник выполнил 10SP из 11SP

		if (team[i].SpBurnt == team[i].SpAll) && (team[j].SpBurnt != team[j].SpAll) {

			if team[i].SpAll/team[j].SpAll > 0.6 {
				return true
			}

		}

		if (team[j].SpBurnt == team[j].SpAll) && (team[i].SpBurnt != team[i].SpAll) {

			if team[j].SpAll/team[i].SpAll > 0.6 {
				return false
			}

		}

		// Далее мы находимся в условии когда оба члена команды закрыли не все что запланировали

		if team[i].SpBurnt > team[j].SpBurnt {
			return true
		}

		// теперь мы находимся в ситуации когда либо меньше сожжено, либо по-ровну
		// в этом случае если сожжено меньше, то стоит отдавать предпочтение тому, у кого сожжено больше

		if team[i].SpBurnt < team[j].SpBurnt {
			return false
		}

		// теперь мы находимся в ситуации когда у обоих сожжено одинаково
		// в этом случае отдаем предпочтение тому, у кого меньше осталось несожженых сп

		if team[i].SpAll > team[j].SpAll {
			return true
		}

		return false

	})

	return team

}

func updateTeamMemberRating(member Member, lastFile string, dir string, statusIndex, spIndex, assigneeIndex int) Member {

	fileData := my_csv.CsvParse(lastFile, dir)

	for i := 0; i < len(fileData); i++ {

		// перебираем каждую строчку последнего файла.
		// передаем в функцию, которая определяет является ли
		// данный член команды участником в этой конкретной задаче

		if isMemberOfThisTask(fileData[i], member, assigneeIndex) {

			member.SpAll += my_conv.ThisStrToFloat(fileData[i][spIndex])

			if fileData[i][statusIndex] == "Закрыт" || fileData[i][statusIndex] == "Закрыто" || fileData[i][statusIndex] == "Выполнено" || fileData[i][statusIndex] == "Отложен" {

				member.SpBurnt += my_conv.ThisStrToFloat(fileData[i][spIndex])

			}

		}

	}

	return member
}

func isMemberOfThisTask(task []string, member Member, assigneeIndex int) bool {

	isException, isMember := checkException(task, member)

	if isException && isMember {
		return true
	} else if isException && !isMember {
		return false
		//} else if strings.Contains(task[6]+task[7], member.Name) {
	} else if strings.Contains(task[assigneeIndex], member.Name) {
		return true
	} else {
		return false
	}
}

func checkException(task []string, member Member) (bool, bool) {
	exceptions := my_csv.CsvParse("exceptions.csv", "data")

	for i := 1; i < len(exceptions); i++ {

		//fmt.Println("запустилась итерация цикла", i)

		// перебираем каждую строчку в файле исключений.

		if exceptions[i][0] == task[0] {
			//fmt.Println("Нашли совпадение\n")
			//fmt.Println("exceptions[i][0]", exceptions[i][0], "\n")
			//fmt.Println("task[0]", task[0], "\n")
			// значит мы нашли задачу данную в исключениях и должны проверить, является ли членом этой задачи переданный на вход член
			if strings.Contains(exceptions[i][1], member.Name) {
				return true, true
			} else {
				return true, false
			}
		}

	}
	return false, false
}

//func ConvertName(nameFromFile string) string {
//	//fmt.Println("nameFromFile")
//	//fmt.Println(nameFromFile)
//	//fmt.Println("nameFromFile end")
//
//	switch nameFromFile {
//	case "KhabibullinEA@region.vtb.ru":
//		return "Эльбек"
//	case "VTB4044850":
//		return "Вася"
//	}
//
//	return nameFromFile
//}
