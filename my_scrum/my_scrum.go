package my_scrum

import (
	"vtb_jira/my_conv"
	"vtb_jira/my_csv"
	"vtb_jira/my_time"
	"fmt"
	"time"
)

func PrintAllBurnDownPoints(allFiles []string, sprintStart, sprintFinish time.Time, dir string) {

	for i := 0; i < len(allFiles); i++ {
		// преобразуем имя файла в дату
		date := my_time.ConvStrToTime(allFiles[i][:len(allFiles[i])-4])

		// получаем сколько SP не хватает до идеального сгорания
		burnDownPoint := int(GetBurnDownPointByFile(allFiles[i], sprintStart, sprintFinish, dir) + 0.5)

		if i == (len(allFiles) - 3) {
			fmt.Printf("позавчера  - %v SP не хватало до идеального ->\n", burnDownPoint)
			continue
		}

		if i == (len(allFiles) - 2) {
			fmt.Printf("вчера      - %v SP не хватало до идеального ->\n", burnDownPoint)
			continue
		}

		if i != (len(allFiles) - 1) {
			fmt.Printf("%v - %v SP не хватало до идеального ->\n", date.Format("02-01-2006"), burnDownPoint)
			continue
		}

		fmt.Printf("сегодня    - %v SP не хватает до идеального\n", burnDownPoint)
	}
}

func GetBurnDownPointByFile(fileName string, sprintStart, sprintFinish time.Time, dir string) float64 {

	fileData := my_csv.CsvParse(fileName, dir)
	scheduledSp := GetScheduledSp(fileData)
	perfectCombastion := GetPerfectCombastion(scheduledSp, sprintStart, sprintFinish, fileName)
	realCombastion := GetRealCombastion(fileData, scheduledSp)
	return realCombastion - perfectCombastion

}

func GetScheduledSp(theFreshestData [][]string) float64 {

	var scheduledSp float64
	scheduledSp = 0

	for i := 1; i < len(theFreshestData); i++ {
		scheduledSp += my_conv.ThisStrToFloat(theFreshestData[i][2])

	}
	//fmt.Println("return scheduled sp", scheduledSp)
	return scheduledSp
}

func GetPerfectCombastion(scheduledSp float64, sprintStart, sprintFinish time.Time, theFreshestFile string) float64 {

	// посчитали общее время спринта в формате duration
	allSprintTime := sprintFinish.Sub(sprintStart)

	// получили текущее время из имени файла
	currentSprintTime, _ := time.Parse("02-01-2006_15-04-05", theFreshestFile[:len(theFreshestFile)-4])

	// получили сколько времени прошло
	currentSprintTimeSpend := sprintFinish.Sub(currentSprintTime)

	// получили строку из формата duration
	a := my_time.ShortDur(currentSprintTimeSpend)

	//получили флоат из строки
	currentSprintHoursSpendFloat := my_conv.ThisStrToFloat(a[:len(a)-1])

	// получили строку сколько часов длится весь спринт
	a = my_time.ShortDur(allSprintTime)

	// перевели ее во флоат
	currentSprintHoursAllFloat := my_conv.ThisStrToFloat(a[:len(a)-1])

	perfectPercentage := (1 * currentSprintHoursSpendFloat) / currentSprintHoursAllFloat

	//fmt.Println("return scheduledSP*perfectPerc", scheduledSp*perfectPercentage)

	return scheduledSp * perfectPercentage
}

func GetRealCombastion(theFreshestData [][]string, scheduledSp float64) float64 {
	var burntSp float64
	burntSp = 0

	for i := 1; i < len(theFreshestData); i++ {

		if theFreshestData[i][3] == "Завершено" || theFreshestData[i][3] == "Complete" {
			burntSp += my_conv.ThisStrToFloat(theFreshestData[i][2])
		}

	}
	return scheduledSp - burntSp
}
