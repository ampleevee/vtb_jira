package my_scrum

import (
	"vtb_jira/dir_functions"
)

func GetDynamics(member Member, dir string, assigneeIndex, statusIndex, spIndex int) []int {

	allCsvFiles := dir_functions.SearchAllCsvFiles(dir)

	var res []int

	for i := 0; i < len(allCsvFiles); i++ {

		res = append(res, GetPlaceInTheFile(member, allCsvFiles, allCsvFiles[i], dir, assigneeIndex, statusIndex, spIndex))

	}

	return res
}

func GetPlaceInTheFile(member Member, allCsvFiles []string, file string, dir string, assigneeIndex, statusIndex, spIndex int) int {

	team := GetSprintTeam(allCsvFiles, dir, assigneeIndex)

	thisFileTeamRating := GetCurrentTeamRating(team, file, dir, statusIndex, spIndex, assigneeIndex)

	for i := 0; i < len(thisFileTeamRating); i++ {
		if member.Name == thisFileTeamRating[i].Name {

			return i + 1
		}
	}

	return 0
}
